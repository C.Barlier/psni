#' Function to normalize the single-cell RNAseq count matrix
#' Each gene expression value is normalized by the maximum expression value taken by the gene across the cells
#'
#' @param x single-cell RNA-seq count data.frame (genes in rows, cells in columns)
#'
#' @return normalized single-cell RNA-seq count data.frame
#' @export
max_norm <-
function(x) {
  y<-sweep(x,MARGIN=1,FUN="/",(as.vector(unlist(apply(x, 1, max)))))
  return(y)
}
