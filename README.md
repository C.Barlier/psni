# PSNI

# PSNI: cell Population-Specific gene regulatory Network Inference from steady-state single cell transcriptomics

PSNI was developed to address key limitations of current GRN inference methods that are designed for developmental processes or time course data, rather than specific GRN for a cell type or cell subtype. 


## INSTALLATIONS

#### Installation of devtools from CRAN

install.packages("devtools")

library("devtools")

#### Installation of PSNI R package using Devtools

install_git("https://gitlab.com/C.Barlier/psni.git")


### EXAMPLES

```R

# Load the packages and dependencies

require(PSNI)
library(reshape2)
library(data.table)

# Load data necessary to run PSNI

# Human ChIP-seq data : data(chip_human)
# Human TFs : data(tfs_human)

# Mouse ChIP-seq data : data(chip_mouse)
# Mouse TFs : data(tfs_mouse)

data(chip_mouse)
data(tfs_mouse)

# transform the tfs table into vector of TFs names or ID: here we go for TFs names
tfs <- tfs_mouse$Symbol

# Load the test data: ESCs dataset (smartseq sequencing)

data(df_data)

# Build the ESCs TRN specific with default parameters
trn_net <- run_PSNI(df_data,chip = chip_mouse,tfsToUse=tfs)

# Build the ESCs large scale GRN specific with default parameters
# 6973 genes are conserved at least in 50% of the cells
grn_net <- run_PSNI(df_data,chip = chip_mouse)
```
